package com.zuitt.batch193;

public class Main {

    public static void main(String[] args){

    User firstUser = new User();
    firstUser.setFirstName("Sean");
    firstUser.setLastName("Joseph");
    firstUser.setAge(25);
    firstUser.setAddress("Manila Philippines");

    System.out.println("User's first name: ");
    System.out.println(firstUser.getFirstName());
    System.out.println("User's last name: ");
    System.out.println(firstUser.getLastName());
    System.out.println("User's age: ");
    System.out.println(firstUser.getAge());
    System.out.println("User's address: ");
    System.out.println(firstUser.getAddress());

    User instructor = new User();
    instructor.setFirstName("Suzy");
    instructor.setLastName("Bae");
    instructor.setAge(36);
    instructor.setAddress("Seoul Korea");

    Course unangKurso = new Course("Java101", "Get started on how to code using Java", 25, 13000, "August 2022", "November 2022", instructor);
    /*unangKurso.setName("Java for Noobs");
    unangKurso.setDescription("Get started on how to code using Java");
    unangKurso.setSeats(25);
    unangKurso.setFee(13000);
    unangKurso.setStartDate("August 2022");
    unangKurso.setEndDate("November 2022");
    unangKurso.setInstructor(instructor);*/

        System.out.println("Course's name: ");
        System.out.println(unangKurso.getName());
        System.out.println("Course's description: ");
        System.out.println(unangKurso.getDescription());
        System.out.println("Course's fee: ");
        System.out.println(unangKurso.getFee());
        System.out.println("Course's instructor: ");
        System.out.println(instructor.getFirstName());

    }



}
