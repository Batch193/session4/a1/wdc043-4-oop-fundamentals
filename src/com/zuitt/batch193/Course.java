package com.zuitt.batch193;

public class Course {

    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private User instructor;

    public Course(){};

    public Course(String name, String description, int seats, double fee, String startDate, String endDate, User instructor){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    };

    //Getters
    public String getName() {
        return this.name;
    }
    public String getDescription() {
        return this.description;
    }
    public int getSeats() {
        return this.seats;
    }
    public double getFee() {
        return this.fee;
    }
    public String getStartDate() {
        return this.startDate;
    }
    public String getEndDate() {
        return this.endDate;
    }
    public User getInstructor() {
        return this.instructor;
    }

    //Setters
    public void setName(String name){
        this.name = name;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setSeats(int seats){
        this.seats = seats;
    }
    public void setFee(double fee){
        this.fee = fee;
    }
    public void setStartDate(String startDate){
        this.startDate = startDate;
    }
    public void setEndDate(String endDate){
        this.endDate = endDate;
    }
    public void setInstructor(User instructor){
        this.instructor = instructor;
    }


}
